defmodule DictionaryTest do
  use ExUnit.Case

  test "start returns a word list" do
    words = Dictionary.start()
    assert Enum.count(words) > 0
  end

  test "random word returns a word" do
    words = Dictionary.start()
    word = Dictionary.random_word(words)
    assert word != nil
    assert String.length(word) > 0
  end
end
